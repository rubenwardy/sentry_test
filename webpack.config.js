const webpack = require("webpack");
const fs = require("fs");
const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ForkTsCheckerPlugin = require("fork-ts-checker-webpack-plugin");

const isProd = process.env.NODE_ENV === "production";
const dest = path.resolve(__dirname, "dist/webext/app");


const configFile = path.resolve(__dirname, "config.json");
function getConfig() {
	const config = JSON.parse(fs.readFileSync(configFile).toString());
	config.SENTRY_DSN = process.env.SENTRY_DSN ?? config.SENTRY_DSN;

	return {
		SENTRY_DSN: JSON.stringify(config.SENTRY_DSN),
	};
}


const mode = isProd ? "production" : "development";

console.log(`Webpack is building in ${mode}`);

module.exports = {
	mode: mode,
	entry: "./src/app/index",
	devtool: "source-map",
	plugins: [
		new webpack.DefinePlugin({
			app_version: {
				version: "\"v1.0.0\"",
				is_debug: "false",
				environment: "\"production\"",
			},
			config: getConfig(),
		}),
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: "[name].css",
			chunkFilename: "[id].css",
		}),
		new HtmlWebpackPlugin({
			filename: "index.html",
			title: "Test",
			template: "src/app/templates/index.ejs",
			hash: true,
		}),
		new ForkTsCheckerPlugin({
			typescript: {
				configFile: path.resolve(__dirname, "tsconfig.json"),
			},
		}),
	],
	module: {
		rules: [
			{
				test: /\.[t|j]sx?$/,
				loader: "babel-loader",
				options: {
					babelrc: false,
					cacheDirectory: true,
					presets: [
						[
							"@babel/preset-typescript",
							{
								isTSX: true,
								allExtensions: true,
							},
						],
						[
							"@babel/preset-env",
							{
								targets: { browsers: ["Chrome 78", "Firefox 70"] },
							},
						],
					],
					plugins: [
						"@babel/transform-react-jsx",
					],
				},
			},
			{
				test: /\.s?[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					"css-loader",
					"sass-loader"
				],
				sideEffects: true,
			},
		],
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"],
		modules: [
			path.resolve(__dirname, "src"),
			"node_modules"
		],
	},
	output: {
		filename: "[name].js",
		path: dest,
		clean: true,
	},
};
