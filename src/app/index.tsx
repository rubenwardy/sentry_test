import React from "react";
import { render } from "react-dom";

import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";


Sentry.init({
	enabled: config.SENTRY_DSN !== undefined,
	dsn: config.SENTRY_DSN,
	integrations: [ new BrowserTracing() ],

	debug: app_version.is_debug,
	environment: app_version.environment,
	release: `sentrytest@${app_version.version}`,

	beforeSend(event) {
		// Drop expected UserError exceptions
		if ((event.exception?.values ?? []).some(x => x.type == "UserError")) {
			return null;
		}

		return event;
	},

	beforeBreadcrumb(crumb) {
		if (crumb.type !== "http") {
			return crumb;
		}

		try {
			const url = new URL(crumb.data!.url);
			for (const key of ["lat", "long"]) {
				if (url.searchParams.has(key)) {
					url.searchParams.set(key, "*****");
				}
			}

			crumb.data!.url = url.toString();
		} catch (e) {
			console.error(e);
		}

		return crumb;
	},
});


function doThing() {
	throw new Error("This is an error!");
}


function ThrowsError() {
	return (<button onClick={() => doThing()}>Throw error</button>);
}


function App() {
	return (<>
		<h1>Hello!</h1>
		<ThrowsError />
	</>);
}



render(
	<App />,
	document.getElementById("app")
);
