interface Version {
	version: string;
	is_debug: boolean;
	environment: string;
}


interface Config {
	SENTRY_DSN?: string;
}

declare let config: Config;
declare const app_version: Version;
