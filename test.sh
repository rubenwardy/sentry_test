#!/bin/bash

VERSION=sentrytest@v1.0.0
export SENTRY_ORG=sentrytest-fb
export SENTRY_PROJECT=sentrytest
export PATH="$PATH:./node_modules/.bin/"

echo "## BUILDING ##"

rm -rf dist
NODE_ENV=production npm run build

echo "## UPLOADING TO SENTRY ##"

sentry-cli releases delete ${VERSION} || true
sentry-cli releases new ${VERSION}
sentry-cli releases deploys ${VERSION} new -e production
sentry-cli releases files ${VERSION} upload-sourcemaps --validate --ext ts --ext js --ext tsx --ext map src dist --url-prefix "~/app"
sentry-cli releases finalize ${VERSION}

echo "## TESTING ##"

rm dist/webext/app/main.js.map
live-server dist/webext/app
